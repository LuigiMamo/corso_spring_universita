package com.homework31Maggio.GestioneEsami.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homework31Maggio.GestioneEsami.models.Studente;

@Service
public class StudenteService {

	@Autowired
	private EntityManager entMan;
	
	private Session getSessione() {
		return entMan.unwrap(Session.class);
	}
	
	public Studente saveStud (Studente objStud) {

		Studente temp = new Studente();			
		temp.setNome(objStud.getNome());
		temp.setCognome(objStud.getCognome());
		temp.setMatricola(objStud.getMatricola());
		temp.setData_nascita(objStud.getData_nascita());

		Session sessione = getSessione();
		try {
			sessione.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

		return temp;
	}
	
	public Studente findById (int varId) {
		Session sessione = getSessione();
		return (Studente) sessione.createCriteria(Studente.class)
								.add(Restrictions.eqOrIsNull("id", varId))
								.uniqueResult();
	}
	
	public List<Studente> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Studente.class).list();
	}
	
	public List<Studente> findByName(String varName){
		Session sessione = getSessione();
		return sessione.createCriteria(Studente.class)
								.add(Restrictions.like("nome", varName, MatchMode.ANYWHERE))
								.list();
		}
	
	public List<Studente> findByNomeCognome(String varNome, String varCognome){
		Session sessione= getSessione();
		return sessione.createCriteria(Studente.class)
								.add(Restrictions.like("nome", varNome, MatchMode.ANYWHERE))
								.add(Restrictions.like("cognome", varCognome, MatchMode.ANYWHERE))
								.list();
	}
	
	@Transactional
	public boolean delete(int varId) {
		Session sessione = getSessione();
		try {
			
			Studente temp = sessione.load(Studente.class, varId);
			
			sessione.delete(temp);
			sessione.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	@Transactional
	public boolean update(Studente objStud) {
		
		Session sessione = getSessione();
		
		try {
			Studente temp = sessione.load(Studente.class, objStud.getId());
			
			if(temp != null) {
				temp.setNome(objStud.getNome());
				temp.setCognome(objStud.getCognome());
				temp.setMatricola(objStud.getMatricola());
				temp.setData_nascita(objStud.getData_nascita());
				
				sessione.update(temp);
				sessione.flush();
				
				return true;
				
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
	return false;
	}
}
