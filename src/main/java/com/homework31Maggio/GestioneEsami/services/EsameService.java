package com.homework31Maggio.GestioneEsami.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import org.hibernate.Session;
import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.homework31Maggio.GestioneEsami.models.Esame;


@Service
public class EsameService {

	@Autowired
	private EntityManager entMan;

	private Session getSessione() {
		return entMan.unwrap(Session.class);

	}

	public Esame saveEsame (Esame objEsame) {

		Esame temp = new Esame();			
		temp.setNome(objEsame.getNome());
		temp.setCodice(objEsame.getCodice());
		temp.setCrediti(objEsame.getCrediti());
		temp.setData_esame(objEsame.getData_esame());

		Session sessione = getSessione();
		try {
			sessione.save(temp);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return null;
		}

		return temp;
	}
	
	public Esame findById (int varId) {
		Session sessione = getSessione();
		return (Esame) sessione.createCriteria(Esame.class)
								.add(Restrictions.eqOrIsNull("id", varId))
								.uniqueResult();
	}
	
	public List<Esame> findByName(String varName){
		Session sessione = getSessione();
		return sessione.createCriteria(Esame.class)
								.add(Restrictions.like("nome", varName, MatchMode.EXACT))
								.list();
	}
	
	public List<Esame> findByData(String varData){
		Session sessione = getSessione();
		return sessione.createCriteria(Esame.class)
								.add(Restrictions.like("data_esame", varData, MatchMode.EXACT))
								.list();
	}
	
	public List<Esame> findAll(){
		Session sessione = getSessione();
		return sessione.createCriteria(Esame.class).list();
	}
	@Transactional
	public boolean delete(int varId) {
		Session sessione = getSessione();
		try {
			
			Esame temp = sessione.load(Esame.class, varId);
			
			sessione.delete(temp);
			sessione.flush();
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return false;
	}
	@Transactional
	public boolean update(Esame objEsame) {
		
		Session sessione = getSessione();
		
		try {
			Esame temp = sessione.load(Esame.class, objEsame.getId());
			
			if(temp != null) {
				temp.setNome(objEsame.getNome());
				temp.setCodice(objEsame.getCodice());
				temp.setCrediti(objEsame.getCrediti());
				temp.setData_esame(objEsame.getData_esame());
				
				sessione.update(temp);
				sessione.flush();
				
				return true;
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());

		}
	return false;
	}
}

	
