package com.homework31Maggio.GestioneEsami;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GestioneEsamiApplication {

	public static void main(String[] args) {
		SpringApplication.run(GestioneEsamiApplication.class, args);
	}

}
