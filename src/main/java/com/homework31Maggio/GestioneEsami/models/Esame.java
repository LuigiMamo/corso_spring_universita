package com.homework31Maggio.GestioneEsami.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="esame")
public class Esame {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name="esameID")
	private int id;
	@Column
	private String nome;
	@Column
	private String codice;
	@Column
	private int crediti;
	@Column
	private String data_esame;
	
	public Esame () {
		
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getCodice() {
		return codice;
	}

	public void setCodice(String codice) {
		this.codice = codice;
	}

	public int getCrediti() {
		return crediti;
	}

	public void setCrediti(int crediti) {
		this.crediti = crediti;
	}

	public String getData_esame() {
		return data_esame;
	}

	public void setData_esame(String data_esame) {
		this.data_esame = data_esame;
	}
	
	
	
	
}
