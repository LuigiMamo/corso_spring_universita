package com.homework31Maggio.GestioneEsami.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.homework31Maggio.GestioneEsami.models.Studente;
import com.homework31Maggio.GestioneEsami.services.StudenteService;

@RestController
@RequestMapping("/studente")
public class StudenteController {

		@Autowired
		private StudenteService service;
		
		@GetMapping("/test")
		public String test() {
			return "Hello World";
		}
	
	@PostMapping("/insert")
	public Studente aggiungi_studente(@RequestBody Studente varStud) {
		return service.saveStud(varStud);
	}
	
	@GetMapping("/{stud_id}")
	public Studente dettaglio_esame(@PathVariable int stud_id) {
		return service.findById(stud_id);
	}
	
	@GetMapping("/cercanome/{stud_name}")
	public List<Studente> cerca_pernome(@PathVariable String stud_name){
		return service.findByName(stud_name);
	}
	
	@GetMapping("/cercanomecompleto/{stud_nome},{stud_cognome}")
	public List<Studente> cerca_nomecompleto(@PathVariable String stud_nome,@PathVariable String stud_cognome){
		return service.findByNomeCognome(stud_nome, stud_cognome);
	}
	
	@GetMapping("/")
	public List<Studente> cerca_studenti(){
		return service.findAll();
	}
	
	@DeleteMapping("/{studente_id}")
	public boolean elimina_esame(@PathVariable int studente_id) {
		return service.delete(studente_id);
	}
	
	@PutMapping("/update") 
	public boolean modifica_esame(@RequestBody Studente varStud) {
		return service.update(varStud);
	}
}
