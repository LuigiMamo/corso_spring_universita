package com.homework31Maggio.GestioneEsami.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.homework31Maggio.GestioneEsami.models.Esame;
import com.homework31Maggio.GestioneEsami.services.EsameService;

@RestController
@RequestMapping("/esame")
public class EsameController {

	@Autowired
	private EsameService service;
	
	@GetMapping("/test")
	public String test() {
		return "Hello World";
	}
	
	@PostMapping("/insert")
	public Esame aggiungi_esame(@RequestBody Esame varEsame) {
		return service.saveEsame(varEsame);
	}
	
	@GetMapping("/{esame_id}")
	public Esame dettaglio_esame(@PathVariable int esame_id) {
		return service.findById(esame_id);
	}
	
	@GetMapping("/cercanome/{esame_name}")
	public List<Esame> cerca_pernome(@PathVariable String esame_name){
		return service.findByName(esame_name);
	}
	
	@GetMapping("/cercadata/{esame_data}")
	public List<Esame> cerca_perdata(@PathVariable String esame_data){
		return service.findByData(esame_data);
	}
	
	@GetMapping("/")
	public List<Esame> cerca_esami(){
		return service.findAll();
	}
	
	@DeleteMapping("/{esame_id}")
	public boolean elimina_esame(@PathVariable int esame_id) {
		return service.delete(esame_id);
	}
	
	@PutMapping("/update") 
	public boolean modifica_esame(@RequestBody Esame varEsame) {
		return service.update(varEsame);
	}
	
	
	
	
}
