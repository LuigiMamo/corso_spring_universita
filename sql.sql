DROP DATABASE IF EXISTS esami;
CREATE DATABASE esami;
USE esami;

CREATE TABLE studente(
	studenteID INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(250) NOT NULL,
    cognome VARCHAR(250) NOT NULL,
    matricola VARCHAR(250) NOT NULL UNIQUE,
    data_nascita DATE,
    PRIMARY KEY(studenteID)
);

CREATE TABLE esame(
	esameID INTEGER NOT NULL AUTO_INCREMENT,
    nome VARCHAR(250) NOT NULL,
    codice VARCHAR(250) NOT NULL,
    crediti INTEGER NOT NULL,
    data_esame DATE,
    PRIMARY KEY(esameID)
);
CREATE TABLE studente_esame(
	studente_rif INTEGER NOT NULL,
    esame_rif INTEGER NOT NULL,
    FOREIGN KEY(studente_rif) REFERENCES studente(studenteID) ON DELETE CASCADE,
    FOREIGN KEY(esame_rif) REFERENCES esame(esameID) ON DELETE CASCADE,
    UNIQUE(studente_rif, esame_rif)
);